Gabby Good and Blake Trossen
Group 5

Welcome to ND Textbooks!

ND Textbooks is a Notre Dame textbook look up, selling, and buying platform
 that combines all useful buying platforms into one. Students are able to
 create an account and advertise a book they are selling. They are also 
able to look up the books their classes require by simply entering the CRN.
 Then, they are able to search for these texts by title or by ISBN, and our
 platform will advertise various buying options, including student listings,
 Notre Dame Bookstore listings, and Amazon listings. Our course data was 
scraped from InsideND, required textbook data and bookstore listings were 
scraped from the Notre Dame Bookstore website via a CherryPy API, and 
Amazon listing data was scraped from Amazon’s website also via a CherryPy 
API. The main advantage of our website is that it integrates all textbook 
selling platforms for Notre Dame students into one platform, as none currently
 exist to compare the many buying options of course textbooks.

All of our code to create the database is hosted in the database folder. All our API code for the ND Bookstore and
Amazon CherryPy APIs is contained in the cherrypy folder. All our PHP and HTML code for the front-end is in the
apache folder. Lastly, our code to scrape HTML files from the ND Bookstore's website is in the bookstore_scraper
folder!

##### MovieController.py #####
# Blake Trossen

import cherrypy
import json

class MovieController:
    # __init__
    def __init__(self, mov_db):
        self.mov_db = mov_db

    # get all movies
    def GET_MOVIES(self):
        resp = {
            "result": "success",
            "movies": []
        }

        for mid in self.mov_db.get_movies():
            mid_info = self.mov_db.get_movie(mid)
            entry = {
                "id":       mid,
                "title":    mid_info[0],
                "genres":   mid_info[1],
                "img":      mid_info[2]
            }

            resp["movies"].append(entry)

        return json.dumps(resp)

    # get a movie by id, return title and description
    def GET_MOVIE(self, movie_id):
        movie_id = int(movie_id)
        info = self.mov_db.get_movie(movie_id)
        if not info:
            return json.dumps({"result": "error"})

        resp = {
            "result":   "success",
            "id":       movie_id,
            "title":    info[0],
            "genres":   info[1],
            "img":      info[2]
        }

        return json.dumps(resp)

    # replace movie, or create it if it doesn't exist
    def PUT_MOVIE(self, movie_id):
        movie_id = int(movie_id)
        msg = json.loads(cherrypy.request.body.read().decode('utf-8'))
        self.mov_db.set_movie(movie_id, [msg["title"], msg["genres"]])
        return json.dumps({"result": "success"})

    # add a new movie
    def POST_MOVIE(self):
        msg = json.loads(cherrypy.request.body.read().decode('utf-8'))
        mids = self.mov_db.get_movies()
        new_mid = mids[-1] + 1
        self.mov_db.set_movie(new_mid, [msg["title"], msg["genres"]])
        return json.dumps({"result": "success", "id": new_mid})

    # delete all movies
    def DELETE_MOVIES(self):
        self.mov_db.movies = {}
        return json.dumps({"result": "success"})

    # delete movie by id
    def DELETE_MOVIE(self, movie_id):
        movie_id = int(movie_id)
        status = self.mov_db.delete_movie(movie_id)
        return json.dumps({"result": "success"})

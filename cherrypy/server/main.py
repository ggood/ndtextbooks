##### main.py #####
# Blake Trossen

import cherrypy
import json
from _movie_database import _movie_database
from MovieController import MovieController
from UserController import UserController
from RatingController import RatingController
from RecController import RecController
from ResetController import ResetController
from OptionsController import OptionsController

### Define CORS Handler Function ###
def CORS():
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, OPTIONS, DELETE"
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"

### Initial Configuration ###
# create new _movie_database
mov_db = _movie_database()

# load all data
mov_db.load_movies('/escnfs/home/cmc/public/paradigms/cherrypy/data/ml-1m/movies.dat')
mov_db.load_users('/escnfs/home/cmc/public/paradigms/cherrypy/data/ml-1m/users.dat')
mov_db.load_ratings('/escnfs/home/cmc/public/paradigms/cherrypy/data/ml-1m/ratings.dat')

# configure server
dispatcher = cherrypy.dispatch.RoutesDispatcher()
conf = {
    'global': {
        'server.socket_host':'student04.cse.nd.edu',
        'server.socket_port':51009
    },
    '/': {
            'request.dispatch': dispatcher,
            'tools.CORS.on': True,
    }
}

# setup CORS response handler
cherrypy.tools.CORS = cherrypy.Tool('before_handler', CORS)

# initialize controllers
MovieController     = MovieController(mov_db)
UserController      = UserController(mov_db)
RatingController    = RatingController(mov_db)
RecController       = RecController(mov_db)
ResetController     = ResetController(mov_db)
OptionsController   = OptionsController()

### Handlers ###
# Movie Handlers
dispatcher.connect('get_movies', '/movies/', controller=MovieController, action='GET_MOVIES', conditions=dict(method=['GET']))
dispatcher.connect('get_movie', '/movies/:movie_id', controller=MovieController, action='GET_MOVIE', conditions=dict(method=['GET']))
dispatcher.connect('put_movie', '/movies/:movie_id', controller=MovieController, action='PUT_MOVIE', conditions=dict(method=['PUT']))
dispatcher.connect('post_movie', '/movies/', controller=MovieController, action='POST_MOVIE', conditions=dict(method=['POST']))
dispatcher.connect('delete_movies', '/movies/', controller=MovieController, action='DELETE_MOVIES', conditions=dict(method=['DELETE']))
dispatcher.connect('delete_movie', '/movies/:movie_id', controller=MovieController, action='DELETE_MOVIE', conditions=dict(method=['DELETE']))

dispatcher.connect('options_movies', '/movies/', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
dispatcher.connect('options_movie', '/movies/:movie_id', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

# User Handlers
dispatcher.connect('get_users', '/users/', controller=UserController, action='GET_USERS', conditions=dict(method=['GET']))
dispatcher.connect('get_user', '/users/:user_id', controller=UserController, action='GET_USER', conditions=dict(method=['GET']))
dispatcher.connect('put_user', '/users/:user_id', controller=UserController, action='PUT_USER', conditions=dict(method=['PUT']))
dispatcher.connect('post_user', '/users/', controller=UserController, action='POST_USER', conditions=dict(method=['POST']))
dispatcher.connect('delete_users', '/users/', controller=UserController, action='DELETE_USERS', conditions=dict(method=['DELETE']))
dispatcher.connect('delete_user', '/users/:user_id', controller=UserController, action='DELETE_USER', conditions=dict(method=['DELETE']))

dispatcher.connect('options_movies', '/users/', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
dispatcher.connect('options_movie', '/users/:user_id', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

# Rating Handlers
dispatcher.connect('get_rating', '/ratings/:movie_id', controller=RatingController, action='GET_RATING', conditions=dict(method=['GET']))

dispatcher.connect('options_rating', '/ratings/:movie_id', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

# Recommendation Handlers
dispatcher.connect('get_rec', '/recommendations/:user_id', controller=RecController, action='GET_REC', conditions=dict(method=['GET']))
dispatcher.connect('put_rec', '/recommendations/:user_id', controller=RecController, action='PUT_REC', conditions=dict(method=['PUT']))
dispatcher.connect('delete_recs', '/recommendations/', controller=RecController, action='DELETE_RECS', conditions=dict(method=['DELETE']))

dispatcher.connect('options_recommendations', '/recommendations/', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
dispatcher.connect('options_recommentdation', '/recommendations/:user_id', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

# Reset Handlers
dispatcher.connect('put_reset_all', '/reset/', controller=ResetController, action='PUT_RESET_ALL', conditions=dict(method=['PUT']))
dispatcher.connect('put_reset', '/reset/:movie_id', controller=ResetController, action='PUT_RESET', conditions=dict(method=['PUT']))

dispatcher.connect('options_reset_all', '/reset/', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
dispatcher.connect('options_reset', '/reset/:movie_id', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

### Run Server ###
# start the server
cherrypy.config.update(conf)
app = cherrypy.tree.mount(None, config=conf)
cherrypy.quickstart(app)

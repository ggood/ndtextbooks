##### RecController.py #####
# Blake Trossen

import cherrypy
import json

class RecController:
    # __init__
    def __init__(self, mov_db):
        self.mov_db = mov_db

    # retrieve movie recommendation for a user
    def GET_REC(self, user_id):
        user_id = int(user_id)
        movie_id = self.mov_db.get_recommendation_for_user(user_id)
        if not movie_id:
            return json.dumps({"result": "failure"})

        return json.dumps({"result": "success", "movie_id": movie_id})

    # add a new vote for a given movie
    def PUT_REC(self, user_id):
        user_id = int(user_id)
        msg = json.loads(cherrypy.request.body.read().decode('utf-8'))
        self.mov_db.set_user_movie_rating(user_id, int(msg["movie_id"]), msg["rating"])
        return json.dumps({"result": "success"})

    # clear votes DB
    def DELETE_RECS(self):
        self.mov_db.ratings = {}
        return json.dumps({"result": "success"})

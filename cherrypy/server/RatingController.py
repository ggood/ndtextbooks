##### RatingController.py #####
# Blake Trossen

import cherrypy
import json

class RatingController:
    # __init__
    def __init__(self, mov_db):
        self.mov_db = mov_db

    # retrieve the average rating for a movie
    def GET_RATING(self, movie_id):
        movie_id = int(movie_id)
        rating = self.mov_db.get_rating(movie_id)
        if rating == 0:
            return json.dumps({"result": "failure"})

        return json.dumps({"result": "success", "movie_id": movie_id, "rating": rating})

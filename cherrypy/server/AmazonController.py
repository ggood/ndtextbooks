##### AmazonController.py #####

import cherrypy
import json

class AmazonController:
    # __init__
    def __init__(self, amzn_api):
        self.amzn = amzn_api

    # get a listing by textboook title
    def GET_LISTINGS(self, query_params):
        print("*****")

        query_params = query_params.split("+")
        params = {}
        for param in query_params:
            param.split("=")
            params[param[0]] = param[1]

        listings = []
        if "title" in params:
            listings = self.amzn.find_listings_by_title(params["title"])
            if not listings:
                return json.dumps({"result": "failure"})

            return json.dumps({"result": "success", "listings": listings})

        if "isbn" in params:
            listings = self.amzn.find_listings_by_isbn(isbn)
            if not listings:
                return json.dumps({"result": "failure"})

            return json.dumps({"result": "success", "listings": listings})

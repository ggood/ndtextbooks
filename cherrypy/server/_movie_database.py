##### _movie_database.py #####
# Blake Trossen

class _movie_database:
    def __init__(self):
        self.movies     = {}
        self.users      = {}
        self.ratings    = {}

    def get_image_url(self, mid):
        image_file = '/escnfs/home/cmc/public/paradigms/cherrypy/data/images.dat'
        image_base_path = 'https://www3.nd.edu/~cmc/teaching/cse30332/images'
        data = open(image_file, "r")
        for line in data:
            parts = line.strip().split('::')
            parts[0] = int(parts[0])
            if parts[0] == mid:
                return parts[2]

        return None

    def load_movies(self, movie_file):
        self.movies = {}
        data = open(movie_file, "r")
        for line in data:
            parts = line.strip().split('::')
            id      = int(parts[0])
            title   = parts[1]
            genres  = parts[2]
            img     = self.get_image_url(id)

            self.movies[id] = {
                "title":    title,
                "genres":   genres,
                "img":      img
            }

    def get_movie(self, mid):
        if mid not in self.movies:
            return None

        return [self.movies[mid]['title'], self.movies[mid]['genres'], self.movies[mid]['img']]

    def get_movies(self):
        return list(self.movies.keys())

    def set_movie(self, mid, info):
        if mid in self.movies:
            img = self.movies[mid]['img']
        else:
            img = ""

        self.movies[mid] = {
            'title':    info[0],
            'genres':   info[1],
            'img':      img
        }

    def delete_movie(self, mid):
        if mid not in self.movies:
            return

        del self.movies[mid]

    def reset_movie(self, mid):
        movie_file = '/escnfs/home/cmc/public/paradigms/cherrypy/data/ml-1m/movies.dat'
        data = open(movie_file, "r")
        for line in data:
            parts = line.strip().split('::')
            id      = int(parts[0])
            if (id == mid):
                title   = parts[1]
                genres  = parts[2]
                img     = self.get_image_url(id)

                self.movies[id] = {
                    "title":    title,
                    "genres":   genres,
                    "img":      img
                }

                return

    def load_users(self, users_file):
        self.users = {}
        data = open(users_file, 'r')
        for line in data:
            parts = line.strip().split("::")
            id          = int(parts[0])
            gender      = parts[1]
            age         = int(parts[2])
            occupation  = int(parts[3])
            zipcode         = parts[4]

            self.users[id] = {
                "gender":       gender,
                "age":          age,
                "occupation":   occupation,
                "zipcode":          zipcode
            }

    def get_user(self, uid):
        if uid not in self.users:
            return None

        gender  = self.users[uid]["gender"]
        age     = self.users[uid]["age"]
        occ     = self.users[uid]["occupation"]
        zipcode = self.users[uid]["zipcode"]
        return [gender, age, occ, zipcode]

    def get_users(self):
        return list(self.users.keys())

    def set_user(self, uid, info):
        self.users[uid] = {
            "gender":       info[0],
            "age":          int(info[1]),
            "occupation":   int(info[2]),
            "zipcode":          info[3]
        }

    def delete_user(self, uid):
        if uid not in self.users:
            return

        del self.users[uid]

    def load_ratings(self, ratings_file):
        self.ratings = {}
        data = open(ratings_file, 'r')
        for line in data:
            parts = line.strip().split('::')
            uid     = int(parts[0])
            mid     = int(parts[1])
            rating  = int(parts[2])

            if mid not in self.ratings.keys():
                self.ratings[mid] = {}

            self.ratings[mid][uid] = rating

    def get_rating(self, mid):
        if mid not in self.ratings:
            return 0

        return sum(self.ratings[mid].values()) / len(self.ratings[mid].values())

    def get_highest_rated_movie(self):
        if not self.ratings:
            return None

        ratings = [(mid, self.get_rating(mid)) for mid in self.ratings.keys()]
        ratings.sort(reverse=True, key=lambda item: item[1])
        return ratings[0][0]

    def set_user_movie_rating(self, uid, mid, rating):
        self.ratings[mid][uid] = rating
        return

    def get_user_movie_rating(self, uid, mid):
        if mid not in self.ratings:
            return None

        if uid not in self.ratings[mid]:
            return None

        return self.ratings[mid][uid]

    def get_recommendation_for_user(self, uid):
        if not self.ratings:
            return None

        ratings = [(mid, self.get_rating(mid)) for mid in self.ratings.keys()]
        ratings.sort(reverse=True, key=lambda item: item[1])
        ratings = [r for r in ratings if r[1] == max([rating[1] for rating in ratings])]
        ratings.sort(key=lambda item: item[0])

        for mid, rating in ratings:
            if self.get_user_movie_rating(uid, mid) == None:
                print(self.get_user_movie_rating(uid, mid))
                return mid

        return None

    def delete_all_ratings(self):
        for mid in self.ratings.keys():
            mid = {}

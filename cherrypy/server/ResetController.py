##### ResetController.py #####
# Blake Trossen

import cherrypy
import json

class ResetController:
    # __init__
    def __init__(self, mov_db):
        self.mov_db = mov_db

    # recreates database from .dat files
    def PUT_RESET_ALL(self):
        self.mov_db.load_movies('/escnfs/home/cmc/public/paradigms/cherrypy/data/ml-1m/movies.dat')
        self.mov_db.load_users('/escnfs/home/cmc/public/paradigms/cherrypy/data/ml-1m/users.dat')
        self.mov_db.load_ratings('/escnfs/home/cmc/public/paradigms/cherrypy/data/ml-1m/ratings.dat')
        return json.dumps({"result": "success"})

    # resets one movie from .dat files
    def PUT_RESET(self, movie_id):
        movie_id = int(movie_id)
        self.mov_db.reset_movie(movie_id)
        return json.dumps({"result": "success"})

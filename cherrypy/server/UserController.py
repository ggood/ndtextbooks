##### UserController.py #####
# Blake Trossen


import cherrypy
import json

class UserController:
    # __init__
    def __init__(self, mov_db):
        self.mov_db = mov_db

    # list all users
    def GET_USERS(self):
        resp = {
            "result": "success",
            "users": []
        }

        for uid in self.mov_db.get_users():
            uid_info = self.mov_db.get_user(uid)
            entry = {
                "id":           uid,
                "age":          uid_info[1],
                "occupation":   uid_info[2],
                "zipcode":      uid_info[3],
                "gender":       uid_info[0]
            }

            resp["users"].append(entry)

        return json.dumps(resp)

    # retrieve user's profile information
    def GET_USER(self, user_id):
        user_id = int(user_id)
        uid_info = self.mov_db.get_user(user_id)
        if not uid_info:
            return json.dumps({"result": "error"})

        resp = {
            "result":       "success",
            "id":           user_id,
            "age":          uid_info[1],
            "occupation":   uid_info[2],
            "zipcode":      uid_info[3],
            "gender":       uid_info[0]
        }

        return json.dumps(resp)

    # replace user, or create if it does not exist
    def PUT_USER(self, user_id):
        user_id = int(user_id)
        msg = json.loads(cherrypy.request.body.read().decode('utf-8'))
        self.mov_db.set_user(user_id, [msg["gender"], msg["age"], msg["occupation"], msg["zipcode"]])
        return json.dumps({"result": "success"})

    # add a new user
    def POST_USER(self):
        msg = json.loads(cherrypy.request.body.read().decode('utf-8'))
        uids = self.mov_db.get_users()
        new_uid = uids[-1] + 1
        self.mov_db.set_user(new_uid, [msg["gender"], msg["age"], msg["occupation"], msg["zipcode"]])
        return json.dumps({"result": "success", "id": new_uid})

    # clear users DB
    def DELETE_USERS(self):
        self.mov_db.users = {}
        return json.dumps({"result": "success"})

    # delete user by id
    def DELETE_USER(self, user_id):
        user_id = int(user_id)
        self.mov_db.delete_user(user_id)
        return json.dumps({"result": "success"})

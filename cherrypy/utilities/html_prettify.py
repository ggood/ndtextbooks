#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup

### Test API: Main Execution ###
if __name__ == '__main__':
    doc_name = "index.html"
    path = "/home/ec2-user/cherrypy/utilities/" + doc_name
    doc = open(path, "r")
    soup = BeautifulSoup(doc, "html.parser")
    print(soup.prettify())

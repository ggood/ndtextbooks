#!/usr/bin/env python3

import cherrypy
import json
import sys

sys.path.append("/home/ec2-user/cherrypy/controllers")
sys.path.append("/home/ec2-user/cherrypy/apis")
from AmazonAPI import AmazonAPI
from BookstoreAPI import BookstoreAPI
from AmazonController import AmazonController
from BookstoreController import BookstoreController
from OptionsController import OptionsController

### Define CORS Handler Function ###
def CORS():
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, OPTIONS, DELETE"
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"

### Initial Configuration ###
# create amazon api object
amzn_api    = AmazonAPI()
bkstr_api   = BookstoreAPI()

# configure server
dispatcher = cherrypy.dispatch.RoutesDispatcher()
conf = {
    'global': {
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 8500,
    },
    '/': {
            'request.dispatch': dispatcher,
            'tools.CORS.on': True,
    }
}

# setup CORS response handler
cherrypy.tools.CORS = cherrypy.Tool('before_handler', CORS)

# initialize controllers
AmazonController    = AmazonController(amzn_api)
BookstoreController = BookstoreController(bkstr_api)
OptionsController   = OptionsController()

### Handlers ###
# Amazon Handlers
dispatcher.connect('get_amazon', '/amazon/:query_params', controller=AmazonController, action='GET_LISTINGS', conditions=dict(method=['GET']))
dispatcher.connect('options_amazon', '/amazon/:query_params', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

# Bookstore Handlers
dispatcher.connect('get_bookstore/required-materials', '/bookstore/required-materials/:query_params', controller=BookstoreController, action='GET_REQUIRED_COURSE_MATERIALS', conditions=dict(method=['GET']))
dispatcher.connect('get_bookstore', '/bookstore/:query_params', controller=BookstoreController, action='GET_LISTINGS', conditions=dict(method=['GET']))

dispatcher.connect('options_bookstore/required-materials', '/bookstore/required-materials/:query_params', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
dispatcher.connect('options_bookstore', '/bookstore/:query_params', controller=OptionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

### Run Server ###
cherrypy.config.update(conf)
app = cherrypy.tree.mount(None, config=conf)
cherrypy.quickstart(app)

##### BookstoreController.py #####

import cherrypy
import json

class BookstoreController:
    # __init__
    def __init__(self, bkstr_api):
        self.bkstr = bkstr_api

    # get listings by course
    def GET_REQUIRED_COURSE_MATERIALS(self, query_params):
        # parse query
        query_params = query_params.strip().split("&")
        params = {}
        for param in query_params:
            param = param.split("=")
            params[param[0]] = param[1]

        # find required materials and return result
        listings = self.bkstr.get_required_course_materials(params["term"], params["dept"], params["course_number"], params["section_number"])
        if listings == None:
            return json.dumps({"result": "failure"})

        return json.dumps({"result": "success", "listings": listings})

    def GET_LISTINGS_BY_COURSE(self, term, dept, course_number, section_number):
        # find listings and return result
        listings = self.bkstr.get_listings_by_course(term, dept, course_number, section_number)
        if listings == None:
            return json.dumps({"result": "failure"})

        return json.dumps({"result": "success", "listings": listings})

    def GET_LISTINGS(self, query_params):
        # parse query
        query_params = query_params.strip().split("&")
        params = {}
        for param in query_params:
            param = param.split("=")
            params[param[0]] = param[1]

        # call appropriate GET handler
        return self.GET_LISTINGS_BY_COURSE(params["term"], params["dept"], params["course_number"], params["section_number"])

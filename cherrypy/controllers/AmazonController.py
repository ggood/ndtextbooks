##### AmazonController.py #####

import cherrypy
import json

class AmazonController:
    # __init__
    def __init__(self, amzn_api):
        self.amzn = amzn_api

    # get a listing by textbook title
    def GET_LISTINGS_BY_TITLE(self, title):
        listings = self.amzn.get_listings_by_title(title)
        if listings == None:
            return json.dumps({"result": "failure"})

        return json.dumps({"result": "success", "listings": listings})

    # get a listing by textbook isbn
    def GET_LISTINGS_BY_ISBN(self, isbn):
        listings = self.amzn.get_listings_by_isbn(isbn)
        if listings == None:
            return json.dumps({"result": "failure"})

        return json.dumps({"result": "success", "listings": listings})

    def GET_LISTINGS(self, query_params):
        if query_params.find("&") >= 0:
            query_params = query_params.strip().split("&")
            query_params = query_params[0]

        query_params = query_params.strip().split("=")
        if query_params[0] == "title":
            return self.GET_LISTINGS_BY_TITLE(query_params[1])

        if query_params[0] == "isbn":
            return self.GET_LISTINGS_BY_ISBN(query_params[1])

        return json.dumps({"result": "failure"})

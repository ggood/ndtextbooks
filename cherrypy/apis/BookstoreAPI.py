#!/usr/bin/env python3

import requests
import copy
import json
from bs4 import BeautifulSoup

class BookstoreAPI:
    def __init__(self):
        self.base_url = "https://www.bkstr.com/webapp/wcs/stores/servlet/"
        self.course_material_appendage = "CourseMaterialsResultsView"
        self.navigation_search_appendage = "NavigationSearch"

        self.base_params = {
            "catalogId":    10001,
            "storeId":      10900,
            "langId":       -1,
        }

        self.term_mapping = {
            "2019_spring": 100056648,
            "2019_summer": 100058904
        }

    def construct_url(self, appendage, params):
        url = self.base_url + appendage + "?"
        for key, val in params.items():
            url += (key + "=" + str(val) + "&")

        return url[:-1]

    def get_required_course_materials(self, term, dept, course_number, section_number):
        params = copy.copy(self.base_params)
        params["programId"]             = 949
        params["categoryId"]            = 9604
        params["termId"]                = self.term_mapping[term]
        params["divisionDisplayName"]   = "%20"
        params["departmentDisplayName"] = dept
        params["courseDisplayName"]     = course_number
        params["sectionDisplayName"]    = section_number
        params["demoKey"]               = "d"
        params["purpose"]               = "browse"

        url = self.construct_url(self.course_material_appendage, params)
        # response = requests.get(url)
        # soup = BeautifulSoup(response.content, 'html.parser')
        # print(soup.prettify())

        saved_html = dept + "_" + course_number + "_" + section_number + ".html"
        saved_html = saved_html.lower()

        try:
            response = open("/home/ec2-user/cherrypy/html/" + saved_html, "r")
        except:
            print("could not find file " + saved_html)
            return None

        soup = BeautifulSoup(response, "html.parser")

        listing_titles      = [title.text.strip() for title in soup.find_all('h3', attrs={'class': 'material-group-title'}) if title.get('class') == ['material-group-title']]
        listing_details     = soup.find_all('form', attrs={'name': 'OrderItemAdd'})

        final_listings = []
        for index, listing in enumerate(listing_details):
            img             = "https:" + listing.find('img')["src"].strip()
            edition         = listing.find('span', attrs={'id': 'materialEdition'}).text.strip().split()[1].strip()
            try:
                isbn            = listing.find('span', attrs={'id': 'materialISBN'}).text.strip().split()[1].strip()
            except AttributeError:
                isbn            = "N/A"
            author          = listing.find('span', attrs={'id': 'materialAuthor'}).text.strip().split()[1].strip()
            table_offers    = listing.find('table', attrs={'role': 'presentation'}).find_all('tr', recursive=False)[1:]

            offers = []
            for table_offer in table_offers:
                offer_info = table_offer.find_all('td')
                type        = offer_info[1].text.strip()
                buy_rent    = offer_info[2].text.strip()
                condition   = offer_info[3].text.strip()
                rent_period = offer_info[4].text.strip()
                due_string  = "Due: "
                due_index   = rent_period.find(due_string)
                if (due_index >= 0):
                    rent_period = "Due " + rent_period[due_index + len(due_string):]

                provider    = offer_info[5].text.strip()
                if offer_info[6].find('span'):
                    in_stock    = True
                else:
                    in_stock    = False

                price       = offer_info[7].text.strip()

                offer = {}
                offer["type"]           = type
                offer["buy_rent"]       = buy_rent
                offer["condition"]      = condition
                offer["rent_period"]    = rent_period
                offer["provider"]       = provider
                offer["in_stock"]       = in_stock
                offer["price"]          = price
                offers.append(offer)

            listing_info = {}
            listing_info["title"]   = listing_titles[index]
            listing_info["img"]     = img
            listing_info["edition"] = edition
            listing_info["isbn"]    = isbn
            listing_info["author"]  = author
            listing_info["offers"]  = offers
            listing_info["link"]    = url
            final_listings.append(listing_info)

        return final_listings

    def get_listings_by_course(self, term, dept, course_number, section_number):
        listings = self.get_required_course_materials(term, dept, course_number, section_number)
        if listings == None:
            return None

        final_listings = []
        for listing in listings:
            offers = []
            for offer in listing["offers"]:
                if offer["in_stock"] == False:
                    continue

                offers.append(offer)

            listing["offers"] = offers
            final_listings.append(listing)

        return final_listings

    def pretty_print(self, listings):
        print("[")
        for listing in listings:
            print(json.dumps(listing, indent=4, sort_keys=False))
            print()
        print("]")

'''
if __name__ == '__main__':
    bkstr = BookstoreAPI()
    listings = bkstr.get_required_course_materials("2019_spring", "ACMS", "10145", "01")
    bkstr.pretty_print(listings)
'''

#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
import json

class AmazonAPI:
    def __init__(self):
        self.base_url = "https://www.amazon.com/"
        self.query_preface  = "s?k="
        self.book_param     = "i=stripbooks"
        self.stop_words = set()
        self.load_stop_words()

    def load_stop_words(self):
        stop_words_file = open("utilities/stop_words.txt", "r")
        stop_words = [line.strip().upper() for line in stop_words_file]
        self.stop_words = set(stop_words)

    def is_valid_result(self, title, response_title):
        title_words = set([word.upper() for word in title.split()])
        additional = set()
        for word in title_words:
            colon = word.find(":")
            if colon >= 0:
                additional.add(word[:colon])

        title_words = title_words.union(additional)

        response_title_words = set([word.upper() for word in response_title.split()])
        additional = set()
        for word in response_title_words:
            colon = word.find(":")
            if colon >= 0:
                additional.add(word[:colon])

        response_title_words = response_title_words.union(additional)

        for word in title_words:
            if word in self.stop_words:
                continue

            if word not in response_title_words:
                print(word, response_title_words)
                return False

        return True

    def get_listings_by_title(self, title):
        url = self.base_url + self.query_preface + "+".join(title.split()) + "&" + self.book_param
        filename = "html/" + title + ".html"

        try:
            response = open(filename, "r")
            soup = BeautifulSoup(response, "html.parser")
            print("LOG -- AMAZON GET_LISTINGS_BY_TITLE: using old file \"{}\"".format(filename))
        except FileNotFoundError:
            outfile = open(filename, "w+")
            print("LOG -- AMAZON GET_LISTINGS_BY_TITLE: creating new file \"{}\"".format(filename))

            headers = {
                "accept"            : "text/html,*/*",
                "accept-encoding"   : "gzip, deflate, br",
                "accept-language"   : "en-US,en;q=0.9",
            }

            response = requests.get(url, headers=headers, verify=False)

            if response.status_code != 200:
                return None

            soup = BeautifulSoup(response.content, 'html.parser')
            outfile.write(soup.prettify())

        response_listings   = soup.find_all('div', attrs={'class': 'sg-col-4-of-12 sg-col-8-of-16 sg-col-16-of-24 sg-col-12-of-20 sg-col-24-of-32 sg-col sg-col-28-of-36 sg-col-20-of-28'})
        response_headers    = soup.find_all('span', attrs={'class': 'rush-component', 'data-component-type': 's-product-image'})

        final_listings = []
        for index, listing in enumerate(response_listings):
            # check to make sure it is a book
            by_check        = listing.find('span', attrs={'class': 'a-size-base'}).text.strip()
            if by_check != "by":
                continue

            # check to make sure that the title matches and result is valid
            result_title    = listing.find('a', attrs={'class': 'a-link-normal a-text-normal'})
            link            = result_title["href"]
            result_title    = listing.find('span', attrs={'class': 'a-size-medium a-color-base a-text-normal'}).text.strip()
            if not self.is_valid_result(title, result_title):
                continue

            # extract information
            listing_info    = {}
            img             = response_headers[index].find('div', attrs={'class': 'a-section aok-relative s-image-fixed-height'}).find('img')["src"]
            authors_row     = listing.find('div', attrs={'class': 'a-row a-size-base a-color-secondary'})
            authors         = [auth.text.strip() for auth in authors_row.find_all('a', attrs={'class': 'a-size-base a-link-normal'})]
            try:
                date        = listing.find('span', attrs={'class': 'a-size-base a-color-secondary a-text-normal'}).text.strip()
            except AttributeError:
                date        = "NULL"

            listing_info["title"]   = result_title
            listing_info["link"]    = self.base_url + link
            listing_info["img"]     = img
            listing_info["authors"] = authors
            listing_info["date"]    = date
            listing_info["offers"]  = {}

            first_offer     = listing.find('div', attrs={'class': 'a-section a-spacing-none a-spacing-top-small'})
            offers          = listing.find_all('div', attrs={'class': 'a-row a-spacing-mini'})
            if (len(offers) > 0):
                offers.insert(0, first_offer)
            elif (first_offer):
                offers = [first_offer]
            else:
                offers = []

            for offer in offers:
                type    = offer.find('a', attrs={'class': 'a-size-base a-link-normal a-text-bold'})
                link    = self.base_url + type["href"]
                type    = type.text.strip()
                try:
                    price   = offer.find('span', attrs={'class': 'a-offscreen'}).text.strip()
                except AttributeError:
                    price   = "NULL"

                listing_info["offers"][type] = [price, link]

            final_listings.append(listing_info)

        return final_listings

    def get_listings_by_isbn(self, isbn):
        url = self.base_url + self.query_preface + "isbn+" + str(isbn) + "&" + self.book_param
        filename = "html/" + isbn + ".html"

        try:
            response = open(filename, "r")
            soup = BeautifulSoup(response, "html.parser")
            print("LOG -- AMAZON GET_LISTINGS_BY_ISBN: using old file \"{}\"".format(filename))
        except FileNotFoundError:
            outfile = open(filename, "w+")
            print("LOG -- AMAZON GET_LISTINGS_BY_ISBN: creating new file \"{}\"".format(filename))

            headers = {
                "accept"            : "text/html,*/*",
                "accept-encoding"   : "gzip, deflate, br",
                "accept-language"   : "en-US,en;q=0.9",
            }

            response = requests.get(url, headers=headers, verify=False)

            if response.status_code != 200:
                return None

            soup = BeautifulSoup(response.content, 'html.parser')
            outfile.write(soup.prettify())

        try:
            response_listings   = soup.find('h1', attrs={'class': 'a-size-medium a-color-base a-text-normal'}).find_all_previous('div', attrs={'class': 'sg-col-4-of-12 sg-col-8-of-16 sg-col-16-of-24 sg-col-12-of-20 sg-col-24-of-32 sg-col sg-col-28-of-36 sg-col-20-of-28'})
            response_headers    = soup.find('h1', attrs={'class': 'a-size-medium a-color-base a-text-normal'}).find_all_previous('span', attrs={'class': 'rush-component', 'data-component-type': 's-product-image'})
        except AttributeError:
            response_listings   = soup.find_all('div', attrs={'class': 'sg-col-4-of-12 sg-col-8-of-16 sg-col-16-of-24 sg-col-12-of-20 sg-col-24-of-32 sg-col sg-col-28-of-36 sg-col-20-of-28'})
            response_headers    = soup.find_all('span', attrs={'class': 'rush-component', 'data-component-type': 's-product-image'})

        final_listings = []
        for index, listing in enumerate(response_listings):
            # extract information
            listing_info    = {}
            result_title    = listing.find('a', attrs={'class': 'a-link-normal a-text-normal'})
            link            = result_title["href"]
            result_title    = listing.find('span', attrs={'class': 'a-size-medium a-color-base a-text-normal'}).text.strip()
            img             = response_headers[index].find('div', attrs={'class': 'a-section aok-relative s-image-fixed-height'}).find('img')["src"]
            authors_row     = listing.find('div', attrs={'class': 'a-row a-size-base a-color-secondary'})
            authors         = [auth.text.strip() for auth in authors_row.find_all('a', attrs={'class': 'a-size-base a-link-normal'})]
            try:
                date        = listing.find('span', attrs={'class': 'a-size-base a-color-secondary a-text-normal'}).text.strip()
            except AttributeError:
                date        = "NULL"

            listing_info["title"]   = result_title
            listing_info["link"]    = self.base_url + link
            listing_info["img"]     = img
            listing_info["authors"] = authors
            listing_info["date"]    = date
            listing_info["offers"]  = {}

            first_offer     = listing.find('div', attrs={'class': 'a-section a-spacing-none a-spacing-top-small'})
            offers          = listing.find_all('div', attrs={'class': 'a-row a-spacing-mini'})
            if (len(offers) > 0):
                offers.insert(0, first_offer)
            elif (first_offer):
                offers = [first_offer]
            else:
                offers = []

            for offer in offers:
                type    = offer.find('a', attrs={'class': 'a-size-base a-link-normal a-text-bold'})
                link    = self.base_url + type["href"]
                type    = type.text.strip()
                try:
                    price   = offer.find('span', attrs={'class': 'a-offscreen'}).text.strip()
                except AttributeError:
                    price   = "NULL"

                listing_info["offers"][type] = [price, link]

            final_listings.append(listing_info)

        return final_listings

    def pretty_print(self, listings):
        print("[")
        for listing in listings:
            print(json.dumps(listing, indent=4, sort_keys=False))
            print()
        print("]")

### Test API: Main Execution ###
'''
if __name__ == '__main__':
    amaz = AmazonAPI()
    listings = amaz.find_listings_by_title("one flew over the cuckoo's nest")
    #listings = amaz.find_listings_by_isbn(9780357700020)

    amaz.pretty_print(listings)

    print()
    listings = amaz.find_listings_by_isbn(9780357700020)
    print(listings)
    session = requests.Session()
    session.trust_env = False
    response = session.get("https://www.amazon.com/s?k=isbn+9780357700020&crid=2XTPEUPPQS29W&sprefix=9780357700020%2Caps%2C156&ref=nb_sb_ss_i_1_13")
    l = open("poop.html", "r")
    soup = BeautifulSoup(l, "html.parser")
    print(soup.prettify())
'''

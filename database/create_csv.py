import csv
import re

def splitalpha(s):
    p = 0
    while p < len(s) and s[p].isalpha():
        p+=1
    return (s[:p], s[p:])

if __name__ == "__main__":
    data = list()
    with open('c.csv') as r_csv:
        csv_reader = csv.reader(r_csv, delimiter=',')
        for line in csv_reader:
            data.append(line)
    with open('courses.csv', mode='w') as w_csv:
        csv_writer = csv.writer(w_csv, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        for line in data:
            course_info = str(line[0])
            x = course_info.split()
            section = x[2]
            c = splitalpha(x[0])
            dept = c[0]
            course_num = c[1]
            name = line[1]
            crn = line[7]
            prof = line[9]
            csv_writer.writerow([crn, course_num, name, dept, section, prof])

load data
INFILE 'courses.csv'
INTO TABLE courses
APPEND
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
(crn, course_num, name, dept, section, prof)

create table users
	(net_id varchar(50) not null primary key,
	password varchar(65) not null,
	last varchar(50) not null,
	first varchar(50) not null,
	email varchar(50) not null,
	phone varchar(12) not null,
	address varchar(100) not null,
	show_phone number(1) not null
	);

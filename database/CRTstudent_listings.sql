create table student_listings
	(listing_id number(5) not null primary key,
	net_id varchar(50) not null,
	isbn number(15) not null,
	title varchar(100) not null,
	author varchar(100) not null, 
	edition varchar(100) not null,
	post_date date not null,
	price number(10) not null,
 	condition varchar(25) not null,
	comments varchar(300) not null,
	sold number(1) not null,
	constraint fk_uid foreign key (net_id) references users(net_id) on delete cascade
);

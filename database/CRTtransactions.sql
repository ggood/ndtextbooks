create table transactions
	(t_id number(8) not null primary key,
	listing_id number(5) not null,
	sell_date date not null,
	constraint fk_lid foreign key (listing_id) references student_listings(listing_id) on delete cascade	
	);

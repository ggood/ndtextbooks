create table courses
	(crn number(5) not null primary key,
	course_num number(5) not null,
	name varchar(50) not null,
	dept varchar(5) not null,
	section varchar(3) not null,
	prof varchar(200) not null);

<?php
session_start();
putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
$conn = oci_connect("group5", "group5", "xe");	
if(!$conn){
	echo("Error Connecting to the database</p>");
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && array_key_exists('netID', $_POST)){
	$netID = $_POST["netID"];
	$pass = $_POST["pass"];
	$hash = hash('sha256', $pass);

	$query = "select * from users where net_id = '" . $netID . "' and password = '" . $hash . "'";
	$stmt = oci_parse($conn, $query);
	if (!$stmt){
		$data["result"] = "DBFailure";
		$data["message"] = "Error parsing query";
	}else{
		$r = oci_execute($stmt);
		if (!$r){
			$data["result"] = "DBFailure";
			$data["message"] = "Error executing query";
		}else{
			if (oci_fetch($stmt)){
				$data["result"] = "Success";
				$_SESSION["net_id"] = $netID;
			}else{
				$data["result"] = "LoginFailure";
				$data["message"] = "No matching user";	
		}
	}	
	$d = json_encode($data);
	echo "$d";
	oci_free_statement($stmt);
	oci_close($conn);
	}
}
?>

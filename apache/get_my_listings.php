<?php
session_start();
putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
$conn = oci_connect("group5", "group5", "xe");
if(!$conn){
	echo("Error Connecting to the database</p>");
}

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	$net_id = $_SESSION["net_id"];
	$query = "select * from student_listings where net_id = '" . $net_id . "' order by sold, listing_id desc";
	$stmt = oci_parse($conn, $query);
	if (!$stmt){
		$data["result"] = "Failure";
		$data["message"] = "Error parsing query";
	}else{
		$r = oci_execute($stmt);
		if (!$r){
			$data["result"] = "Failure";
			$data["message"] = "Error executing query";
		}else{
			$i = 0;
			$listings = array();
			while($row = oci_fetch_array($stmt)){
				$listings[$i]->listing_id = $row['LISTING_ID'];
				$listings[$i]->seller_net_id = $row['NET_ID'];
				$listings[$i]->isbn = $row['ISBN'];
				$listings[$i]->title = $row['TITLE'];
				$listings[$i]->author = $row['AUTHOR'];
				$listings[$i]->edition = $row['EDITION'];
				$listings[$i]->post_date = $row['POST_DATE'];
				$listings[$i]->price = $row['PRICE'];
				$listings[$i]->condition = $row['CONDITION'];
				$listings[$i]->comments = $row['COMMENTS'];
				$listings[$i]->sold = $row['SOLD'];
				if($row['SOLD'] == 1){	
					$t_query = "select sell_date from transactions where listing_id = ". $row['LISTING_ID'];
					$t_stmt = oci_parse($conn, $t_query);
					$t_r = oci_execute($t_stmt);
					$t_row = oci_fetch_array($t_stmt);
					$listings[$i]->sell_date = $t_row['SELL_DATE'];
				}
				$i = $i + 1;
			}
			if(!$row && ($i = 0)){
				$data["result"] = "Failure";
				$data["message"] = "Database found no matching listings";
			}else{
				$data->result = "Success";
				$data->listings = $listings;
			}
		}
	}
	$s = json_encode($data);
	echo "$s";
	oci_free_statement($stmt);
	oci_close($conn);
}
?>

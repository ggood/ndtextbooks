<?php
session_start();
putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
$conn = oci_connect("group5", "group5", "xe");	
if(!$conn){
	echo("Error Connecting to the database</p>");
}

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	$l_id = $_POST["l_id"];
	$l_arr = explode(' ', $l_id);
	foreach ($l_arr as $l_id) {
		$query = "update student_listings set sold = 1 where listing_id = " . $l_id;
		$stmt = oci_parse($conn, $query);
		if (!$stmt){
			$data["update_result"] = "DBFailure";
			$data["update_message"] = "Error parsing query";
		}else{
			$r = oci_execute($stmt);
			if (!$r){
				$data["update_result"] = "DBFailure";
				$data["update_message"] = "Error executing query";
			}else{
				$data["update_result"] = "Success";
			}
		}
		//get date in YYYY-MM-DD
		$date = date("Y-m-d");
		$squery = "select transactions_seq.nextval from dual";
		$s_stmt = oci_parse($conn, $squery);
		$r = oci_execute($s_stmt);
		$t = oci_fetch_array($s_stmt);
		$t_id = $t["NEXTVAL"];
		$query2 = "insert into transactions values (" . $t_id . ", " . $l_id . ", TO_DATE('" . $date . "', 'yyyy-mm-dd'))";
		$stmt2 = oci_parse($conn, $query2);
		if (!$stmt2){
			$data["trans_result"] = "DBFailure";
			$data["trans_message"] = "Error parsing query";
		}else{
			$r2 = oci_execute($stmt2);
			if (!$r2){
				$data["trans_result"] = "DBFailure";
				$data["trans_message"] = "Error executing query";
			}else{
				$data["trans_result"] = "Success";
			}
		}
	}	
	$d = json_encode($data);
	echo "$d";
	oci_free_statement($stmt);
	oci_close($conn);
}
?>

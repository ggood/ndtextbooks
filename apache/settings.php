<?php
session_start();
putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
$conn = oci_connect("group5", "group5", "xe");	
if(!$conn){
	echo("Error Connecting to the database</p>");
}
if ($_SERVER['REQUEST_METHOD'] == 'POST' && array_key_exists('type', $_POST)){
	$type = $_POST["type"];
	$skip = 0;
	if ($type == 'login'){	
		$old = $_POST["old"];
		$new = $_POST["new1"];
		$old_hash = hash('sha256', $old);
		$new_hash = hash('sha256', $new);
		//check that old password matches	
		$net_id = $_SESSION["net_id"];
		$query = "select password from users where net_id = '" . $net_id . "'";
		$stmt = oci_parse($conn, $query);
		if (!$stmt){
			$data["result"] = "Failure";
			$data["message"] = "Error parsing password query";
		}else{
			$r = oci_execute($stmt);
			if (!$r){
				$data["result"] = "Failure";
				$data["message"] = "Error executing password query";
			}else{
				$row = oci_fetch_array($stmt);
				if($row['PASSWORD'] != $old_hash){
					$data["result"] = "Failure";
					$data["message"] = "Old password is incorrect";
					$skip = 1;
				}
			}
		}
		if($skip == 0){	
			$query1 = "update users set password = '" . $new_hash . "' where net_id = " . $net_id;
			$stmt1 = oci_parse($conn, $query1);
			if (!$stmt1){
				$data["result"] = "Failure";
				$data["message"] = "Error parsing update query";
			}else{
				$r = oci_execute($stmt);
				if (!$r){
					$data["result"] = "Failure";
					$data["message"] = "Error executing update query";
				}else{
					$data["result"] = "Success";
				}
			}
		}	
	}else{
		$phone = $_POST["phone"];
		$addr = $_POST["addr"];
		$show_phone = $_POST["showPhone"];
		$net_id = "'" . $_SESSION["net_id"] . "'";
		if($phone != '' && $addr != ''){
			$query2 = "update users set phone = " . $phone . ", address = '" . $addr . "', show_phone = " . $show_phone . " where net_id = " . $net_id;
		}else if($phone != ''){
			$query2 = "update users set phone = " . $phone . ", show_phone = " . $show_phone .  " where net_id = " . $net_id;
		}else if($addr != ''){
			$query2 = "update users set address = '" . $addr . "', show_phone = " . $show_phone . " where net_id = " . $net_id;
		}else{
			$query2 = "update users set show_phone = " . $show_phone . " where net_id = " . $net_id;
		}
		$stmt2 = oci_parse($conn, $query2);
		if (!$stmt2){
			$data["result"] = "Failure";
			$data["message"] = "Error parsing update query";
		}else{
			$r = oci_execute($stmt2);
			if (!$r){
				$data["result"] = "Failure";
				$data["message"] = "Error executing update query";
			}else{
				$data["result"] = "Success";
			}
		}
	}
	$d = json_encode($data);
	echo "$d";
	oci_free_statement($stmt);
	oci_close($conn);
}
?>

<?php
session_start();
putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
$conn = oci_connect("group5", "group5", "xe");	
if(!$conn){
	echo("Error Connecting to the database</p>");
}
if ($_SERVER['REQUEST_METHOD'] == 'POST' && array_key_exists('isbn', $_POST)){
	$isbn = $_POST["isbn"];
	$title = $_POST["title"];
	$authors = $_POST["authors"];
	$edition = $_POST["edition"];
	$quality = $_POST["quality"];
	$comments = $_POST["comments"];
	$price = $_POST["price"];
	$netid = $_SESSION["net_id"];
	$sold = 0;
	//gets date in YYYY-MM-DD
	$post_date = date("Y-m-d");
	$query1 = "select listings_seq.nextval from dual";
	$stmt = oci_parse($conn, $query1);
	if (!$stmt){
		$data["result"] = "Failure";
		$data["message"] = "Error getting listing ID";
	}else{
		$r = oci_execute($stmt);
		$l = oci_fetch_array($stmt);
		$l_id = $l['NEXTVAL'];
	}
	$query2 = "insert into student_listings values(" . $l_id . ", '" . $netid ."', " . $isbn . ", " . $title . ", " . $authors . ", " . $edition . ", TO_DATE('" . $post_date . "', 'yyyy-mm-dd'), " . $price . ", " . $quality . ", " . $comments . ", " . $sold . ")";
	$stmt = oci_parse($conn, $query2);
	if (!$stmt){
		$data["result"] = "Failure";
		$data["message"] = "Error parsing query";
	}else{
		$r = oci_execute($stmt);
		if (!$r){
			$data["result"] = "Failure";
			$data["message"] = "Error executing query";
		}else{
			$data["result"] = "Success";
		}
	}	
	$d = json_encode($data);
	echo "$d";
	oci_free_statement($stmt);
	oci_close($conn);
}
?>

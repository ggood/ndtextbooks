<?php

putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
$conn = oci_connect("group5", "group5", "xe");	
if(!$conn){
	echo("Error Connecting to the database</p>");
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && array_key_exists("crn", $_POST)){
	$crn = $_POST["crn"];
	$query = "select * from courses where crn = " . $crn;
//	echo $query;	
	$stmt = oci_parse($conn, $query);
	if (!$stmt){
		$data["result"] = "Failure";
		$data["message"] = "Error parsing query";
	}else{
		$r = oci_execute($stmt);
		if (!$r){
			$data["result"] = "Failure";
			$data["message"] = "Error executing query";
		}else{
			$row = oci_fetch_array($stmt);
			if(!$row){
				$data["result"] = "Failure";
				$data["message"] = "CRN does not exist";
			}else{	
				$data["term"] = "spring 2019";
				$data["course_number"] = $row['COURSE_NUM'];
				$data["dept"] = $row['DEPT'];
				$data["section_number"] = $row['SECTION'];
				$data["result"] = "Success";
			}
		}
	}	
	$d = json_encode($data);
	echo "$d";
	oci_free_statement($stmt);
	oci_close($conn);
}
?>

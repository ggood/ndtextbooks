<?php
session_start();
putenv("ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe/");
$conn = oci_connect("group5", "group5", "xe");	
if(!$conn){
	echo("Error Connecting to the database</p>");
}

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	$netID = $_POST["net_id"];
	$query = "select * from users where net_id = " . $netID;
	$stmt = oci_parse($conn, $query);
	if (!$stmt){
		$data["result"] = "DBFailure";
		$data["message"] = "Error parsing query";
	}else{
		$r = oci_execute($stmt);
		if (!$r){
			$data["result"] = "DBFailure";
			$data["message"] = "Error executing query";
		}else{
			$row = oci_fetch_array($stmt);
			if($row){
				$data["result"] = "Success";
				$data["email"] = $row["EMAIL"];
				$data["first"] = $row["FIRST"];
				$data["last"] = $row["LAST"];
				$data["phone"] = $row["PHONE"];
				$data["show_phone"] = $row["SHOW_PHONE"];
			}else{
				$data["result"] = "LoginFailure";
				$data["message"] = "No matching user";	
			}
		}
	}	
	$d = json_encode($data);
	echo "$d";
	oci_free_statement($stmt);
	oci_close($conn);
}
?>

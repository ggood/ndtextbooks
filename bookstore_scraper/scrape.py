#!/usr/bin/env python3

'''
This program downloads HTML from the bookstore website until the number
of requests made to the site exceeds that which the browser cookie can make
to the site (usually around 58).

To run again, simply close browser application completely, navigate to the site,
and replace the cookie in the header with the new cookie given.
'''

import requests
import copy
import json
from bs4 import BeautifulSoup
import re

class BookstoreAPI:
    def __init__(self):
        self.base_url = "https://www.bkstr.com/webapp/wcs/stores/servlet/"
        self.course_material_appendage = "CourseMaterialsResultsView"
        self.navigation_search_appendage = "NavigationSearch"

        self.base_params = {
            "catalogId":    10001,
            "storeId":      10900,
            "langId":       -1,
        }

        self.term_mapping = {
            "2019_spring": 100056648,
            "2019_summer": 100058904
        }

    def construct_url(self, appendage, params):
        url = self.base_url + appendage + "?"
        for key, val in params.items():
            url += (key + "=" + str(val) + "&")

        return url[:-1]

    def get_url(self, term, dept, course_number, section_number):
        params = copy.copy(self.base_params)
        params["programId"]             = 949
        params["categoryId"]            = 9604
        params["termId"]                = self.term_mapping[term]
        params["divisionDisplayName"]   = "%20"
        params["departmentDisplayName"] = dept
        params["courseDisplayName"]     = course_number
        params["sectionDisplayName"]    = section_number
        params["demoKey"]               = "d"
        params["purpose"]               = "browse"

        url = self.construct_url(self.course_material_appendage, params)
        return url

    def get_required_course_materials(self, term, dept, course_number, section_number):
        params = copy.copy(self.base_params)
        params["programId"]             = 949
        params["categoryId"]            = 9604
        params["termId"]                = self.term_mapping[term]
        params["divisionDisplayName"]   = "%20"
        params["departmentDisplayName"] = dept
        params["courseDisplayName"]     = course_number
        params["sectionDisplayName"]    = section_number
        params["demoKey"]               = "d"
        params["purpose"]               = "browse"

        url = self.construct_url(self.course_material_appendage, params)

        saved_html = dept + "_" + course_number + "_" + section_number + ".html"
        saved_html = saved_html.lower()

        try:
            response = open("/home/ec2-user/cherrypy/html/" + saved_html, "r")
        except:
            return None

        soup = BeautifulSoup(response, "html.parser")
        listing_titles      = [title.text.strip() for title in soup.find_all('h3', attrs={'class': 'material-group-title'}) if title.get('class') == ['material-group-title']]
        listing_details     = soup.find_all('form', attrs={'name': 'OrderItemAdd'})

        final_listings = []
        for index, listing in enumerate(listing_details):
            img             = "https:" + listing.find('img')["src"].strip()
            edition         = listing.find('span', attrs={'id': 'materialEdition'}).text.strip().split()[1].strip()
            isbn            = listing.find('span', attrs={'id': 'materialISBN'}).text.strip().split()[1].strip()
            author          = listing.find('span', attrs={'id': 'materialAuthor'}).text.strip().split()[1].strip()
            table_offers    = listing.find('table', attrs={'role': 'presentation'}).find_all('tr', recursive=False)[1:]

            offers = []
            for table_offer in table_offers:
                offer_info = table_offer.find_all('td')
                type        = offer_info[1].text.strip()
                buy_rent    = offer_info[2].text.strip()
                condition   = offer_info[3].text.strip()
                rent_period = offer_info[4].text.strip()
                due_string  = "Due: "
                due_index   = rent_period.find(due_string)
                if (due_index >= 0):
                    rent_period = "Due " + rent_period[due_index + len(due_string):]

                provider    = offer_info[5].text.strip()
                if offer_info[6].find('span'):
                    in_stock    = True
                else:
                    in_stock    = False

                price       = offer_info[7].text.strip()

                offer = {}
                offer["type"]           = type
                offer["buy_rent"]       = buy_rent
                offer["condition"]      = condition
                offer["rent_period"]    = rent_period
                offer["provider"]       = provider
                offer["in_stock"]       = in_stock
                offer["price"]          = price
                offers.append(offer)

            listing_info = {}
            listing_info["title"]   = listing_titles[index]
            listing_info["img"]     = img
            listing_info["edition"] = edition
            listing_info["isbn"]    = isbn
            listing_info["author"]  = author
            listing_info["offers"]  = offers
            listing_info["link"]    = url
            final_listings.append(listing_info)

        return final_listings

    def get_listings_by_course(self, term, dept, course_number, section_number):
        listings = self.get_required_course_materials(term, dept, course_number, section_number)
        if listings == None:
            return None

        final_listings = []
        for listing in listings:
            offers = []
            for offer in listing["offers"]:
                if offer["in_stock"] == False:
                    continue

                offers.append(offer)

            listing["offers"] = offers
            final_listings.append(listing)

        return final_listings

    def pretty_print(self, listings):
        print("[")
        for listing in listings:
            print(json.dumps(listing, indent=4, sort_keys=False))
            print()
        print("]")


if __name__ == '__main__':
    coursesfile = "courses.csv"
    courses = open(coursesfile, "r")

    for line in courses:
        line = line.strip().split(",")
        dept            = line[3][1:-1]
        course_number   = line[1][1:-1]
        section_number  = line[4][1:-1]

        bkstr = BookstoreAPI()
        url = bkstr.get_url("2019_spring", dept, course_number, section_number)

        fname = dept + "_" + course_number + "_" + section_number + ".html"
        out = open("html/" + fname.lower(), "w+")

        headers = {
            'accept'            : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding'   : 'gzip, deflate, br',
            'accept-language'   : 'en-US,en;q=0.9',
            'cache-control'     : 'max-age=0',

            'cookie'        : 'TLTUID=F18018F8E5B48B5AADAE350694E89D97; _gcl_au=1.1.489490484.1555705553; _ga=GA1.2.1968383575.1555705553; RES_TRACKINGID=63505438635550755; ResonanceSegment=1; _fbp=fb.1.1555705554117.1300890221; dtm_token=AQEAlaq9KORMDAF2SIhoAQEI-wE%22; WC_PERSISTENT=TeBc39J7%2b8SVbME%2fzh47cTSYdag%3d%0a%3b2019%2d04%2d19+15%3a26%3a06%2e1%5f1555705551026%2d201474%5f10900%5f%2d1002%2c%2d1%2cUSD%5f10900; AKA_A2=A; AMCV_712B7B2A53ABFF670A490D45%40AdobeOrg=283337926%7CMCIDTS%7C18020%7CMCMID%7C07899288285962145290761969487050959693%7CMCAAMLH-1557016126%7C7%7CMCAAMB-1557516514%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCAID%7CNONE; _gid=GA1.2.1271048197.1556911714; RES_SESSIONID=30280278841711965; JSESSIONID=0000mX6lgYijV2tM6j7nTcBXbpL:199padae5; REFERRER=https%3a%2f%2fwww%2egoogle%2ecom%2f; EFOLLETT_USER_CART={"userInfo":{"username":""#"ut":"G"#"uid":"-1002"}#"cartItemCount":"0"#"preOrderItemCount":"0"}; UnsupportedBrowser=false; TLTSID=F253007C8F9F67635F5A7EB443E8E2C6; _gcl_aw=GCL.1556914614.EAIaIQobChMI46mp75aA4gIVBI9bCh2mQAr2EAAYASAAEgKVkvD_BwE; _gcl_dc=GCL.1556914614.EAIaIQobChMI46mp75aA4gIVBI9bCh2mQAr2EAAYASAAEgKVkvD_BwE; _sdsat_common.follettStoreID=700; _sdsat_common.userID=-1002; IR_gbd=bkstr.com; IR_PI=7bdb212a-07ee-11e9-8ccb-0236c10c7d28%7C1557001014348; _sdsat_common.storeName=Hammes Notre Dame Bookstore in the Eck Center; _sdsat_tlSessionId=F253007C8F9F67635F5A7EB443E8E2C6; _sdsat_common.userType=G; _sdsat_common.personalizationID=1555705551026-201474; _sdsat_common.schoolName=University Of Notre Dame; s_campaign=RisePaidSearch-_-NonBrndSearchSchool%20Specific%20Notre%20DameExact-_-Google-_-Store%20700; productnum=17; s_cc=true; _gac_UA-55855924-1=1.1556914615.EAIaIQobChMI46mp75aA4gIVBI9bCh2mQAr2EAAYASAAEgKVkvD_BwE; _sdsat_platform=Desktop; _sdsat_common.schoolImg=https://images.efollett.com/htmlroot/images/templates/storeLogos/CA/700.gif; WC_SESSION_ESTABLISHED=true; WC_AUTHENTICATION_-1002=%2d1002%2cjRymmM%2fFBFiUI3ugKNJbe5MTgzo%3d; WC_ACTIVEPOINTER=%2d1%2c10900; WC_USERACTIVITY_-1002=%2d1002%2c10900%2cnull%2cnull%2cnull%2cnull%2cnull%2cnull%2cnull%2cnull%2cGn4u4KrvgkKzyyGmrAORV50X9jxMoo2CqAXYnFnbG0le4exrKVSTGO%2b80OLwJr6WZw1GhFTRf6Ki%0aq5j9e7phw1d%2be72mJAsNuktbPFFkTQdMG1L0KF6ylUZsMEyoJsQ1KDGOYiFhcGy3UD1uO1T7L4%2bo%0aaIJP6v5VPMZ1%2fxvJ4vH8Y2UeoGdN1X2fo2J7ggTK6c1GyKwhSrKVNXxlQr7Y4A%3d%3d; WC_GENERIC_ACTIVITYDATA=[8806263929%3atrue%3afalse%3a0%3aj0mL0KjZfXcJlvrXo8VJQ4uURPM%3d][com.ibm.commerce.context.audit.AuditContext|1555705551026%2d201474][com.ibm.commerce.store.facade.server.context.StoreGeoCodeContext|null%26null%26null%26null%26null%26null][CTXSETNAME|Store][com.ibm.commerce.context.globalization.GlobalizationContext|%2d1%26USD%26%2d1%26USD][com.ibm.commerce.catalog.businesscontext.CatalogContext|10001%26null%26false%26false%26false][com.ibm.commerce.context.base.BaseContext|10900%26%2d1002%26%2d1002%26%2d1][com.ibm.commerce.context.experiment.ExperimentContext|null][com.ibm.commerce.context.entitlement.EntitlementContext|11708%2611708%26null%26%2d2000%26null%26null%26null][com.ibm.commerce.giftcenter.context.GiftCenterContext|null%26null%26null]; IR_2379=1556914617308%7C177366%7C1556914614348%7C%7C; IRMS_la2379=1556914617378; s_ppn=700%3ATextbooks%20-%20Select%20by%20DDCS; s_ppvl=700%3ABookstore%20Home%20Page%2C30%2C25%2C873%2C1680%2C873%2C1680%2C1050%2C2%2CL; __evuid=zzzf74f7003634c3e2119a522d18b3f1:5c8:ecfedfd83ca06f2ead84cf2eb798778e; s_ppv=700%3ATextbooks%20-%20Select%20by%20DDCS%2C88%2C79%2C966%2C863%2C873%2C1680%2C1050%2C2%2CLP; s_sq=folglobalprod%3D%2526c.%2526a.%2526activitymap.%2526page%253D700%25253ATextbooks%252520-%252520Select%252520by%252520DDCS%2526link%253DSubmit%2526region%253DsubmitButtonDiv%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253D700%25253ATextbooks%252520-%252520Select%252520by%252520DDCS%2526pidt%253D1%2526oid%253Dfunctiononclick%252528event%252529%25257BsubmitSection%252528%252529%25257D%2526oidt%253D2%2526ot%253DSUBMIT; SHOP_BY={"tabSelected":"ShopByCourse","modalClosed":"undefined","storeID":""}; dtPC=514617135_371h24; dtCookie=65F7311B6223D67D0B775D6635A93EB2|X2RlZmF1bHR8MQ; dtSa=false%7CC%7C24%7CSubmit%7Ct-100%5Ec%20x%7C1556914776526%7C514617135_371%7Chttps%3A%2F%2Fwww.bkstr.com%2Fnotredamestore%2Fshop%2Ftextbooks-and-course-materials%7CNotre%20Dame%20Textbooks%20%5Ep%20New%5Ec%20Used%5Ec%20Rental%20and%20Digital%20Textbooks%7C1556914622140%7C; ak_bmsc=3750FCBC3401C06F92D6E136792E9C8017DC6014B46B00006096CC5C1EA16B0A~pl1MBo8W89VjoqA5G0/3OAbrX0SvzBQadEYtJWEY1lOFs+I/QA6zvP0QdEe8G7DM7blaXG1LXfp9f1i669oIHuz9UBBVAe3Jza0QYhg1NLzxjxuiNiBQr8QBPQQ7haaUfElVwuViw5HbqteZfKz3KVrpfBDsmUlE05/+eRKXC++QKn3HR9FFAsKYXzIeePTu6HvR9E4tijnZgsrse4phbmyL8OyZCy29RuCzN5WGB6UvGPOK9+SD0WziFmo9q5oybEIVPQpU9loBZlhBnh5ZGWmQ==; dtLatC=38',

            'referer'   : 'https://www.bkstr.com/notredamestore/shop/textbooks-and-course-materials',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
        }

        response = requests.get(url, headers=headers)
        if (response.status_code != 200):
            break

        if (re.search("You have reached the maximum number of course searches", response.text) != None):
            print("sad! bookstore has blocked us :(")
            break

        out.write(response.text)
        print(fname)

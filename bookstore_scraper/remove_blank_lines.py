#!/usr/bin/env python3

'''
This file removes blank lines from html pages, as the bookstore HTML has a
lot of blank lines that can mess up the bs4 functionality for the API, plus
the blank lines take up unecessary space.
'''

import re

if __name__ == '__main__':
    for filename in sys.stdin:
        filename = filename.strip()
        sourcefilename      = "/home/ec2-user/bookstore_html/" + filename
        sourcefile          = open(sourcefilename, "r")
        destinationfilename = "/home/ec2-user/modify_html/" + filename
        destinationfile     = open(destinationfilename, "w+")

        for line in sourcefile:
            if (re.search('[^\\t^\\n^\\s]', line) == None):
                continue

            destinationfile.write(line)

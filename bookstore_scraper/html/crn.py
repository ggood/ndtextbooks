#!/usr/bin/env python3

import re

if __name__ == '__main__':
    filename = "courses_scraped.txt"
    file = open(filename, "r")
    crns = open("crns.txt", "w+")

    for line in file:
        line = line.strip()
        pos = line.find(".html")
        if pos >= 0:
            line = line[:pos]

        info = line.split("_")
        dept = str(info[0].upper().strip())
        num = info[1]
        sect = info[2]

        courses = open("courses.csv", "r")
        for c in courses:
            c = c.strip().split(",")
            n = str(c[1][1:-1].strip())
            d = str(c[3][1:-1].strip())
            s = str(c[4][1:-1].strip())

            if dept == d and num == n and sect == s:
                crn = c[0][1:-1]
                out = crn + "\t" + dept + "-" + num + "-" + sect + "\n"
                crns.write(out)
                break

#!/usr/bin/env python3

'''
This script finds the crns for the courses that were scraped from the
bookstore website and generates "compatible_courses.txt," which contains a
list of all courses that NDBookstore is compatible with and the CRNs of each
course.
'''

if __name__ == '__main__':
    filename = "courses_scraped.txt"
    file = open(filename, "r")
    crns = open("compatible_courses.txt", "w+")

    for line in file:
        line = line.strip()
        pos = line.find(".html")
        if pos >= 0:
            line = line[:pos]

        info = line.split("_")
        dept = str(info[0].upper().strip())
        num = info[1]
        try:
            sect = info[2]
        except:
            print(line)

        courses = open("courses.csv", "r")
        for c in courses:
            c = c.strip().split(",")
            n = str(c[1][1:-1].strip())
            d = str(c[3][1:-1].strip())
            s = str(c[4][1:-1].strip())

            if dept == d and num == n and sect == s:
                crn = c[0][1:-1]
                out = crn + "\t" + dept + "-" + num + "-" + sect + "\n"
                crns.write(out)
                break
